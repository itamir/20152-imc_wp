﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }


        private async void calcular_Click(object sender, RoutedEventArgs e)
        {
            if (!pesoBox.Text.ToString().Trim().Equals("") && !alturaBox.Text.ToString().Trim().Equals(""))
            {
                double peso = Convert.ToDouble(pesoBox.Text.ToString());
                double altura = Convert.ToDouble(alturaBox.Text.ToString());

                double imc = peso / Math.Pow(altura, 2);

                string resultado = "";
                
                if(imc < 16)
                {
                    resultado = "Baixo peso muito grave.";
                } else if(imc >= 16 && imc <=16.99)
                {
                    resultado = "Baixo peso grave.";
                } else if (imc >= 17 && imc <= 18.49)
                {
                    resultado = "Baixo peso.";
                } else if(imc >= 18.50 && imc <= 24.99)
                {
                    resultado = "Peso normal.";
                } else if(imc >= 25 && imc <= 29.99)
                {
                    resultado = "Sobrepeso.";
                } else if (imc >= 30 && imc <= 34.99)
                {
                    resultado = "Obesidade Grau I.";
                } else if (imc >= 35 && imc <= 39.99)
                {
                    resultado = "Obesidade Grau II.";
                } else if( imc >=  40)
                {
                    resultado = "Obesidade Grau III (mórbida).";
                }

                resultadoText.Text = resultado;
            }
            else
            {
                MessageDialog msg = new MessageDialog("Digite valores válidos.", "IMC");
                await msg.ShowAsync();
            }
        }
    }
}
